/* global window, document, Image, jQuery */
/* jshint devel:true */

/*! @license credits */
(function (window, document, $) {
  'use strict';

  /**
   * Debouncer utility
   * @param  {function} func
   * @param  {int}      timeoutArg
   * @return {function}
   */
  function debouncer (func, timeoutArg) {
    var timeoutID;
    var timeout = timeoutArg || 200;
    return function () {
      var scope = this;
      var args = arguments;
      clearTimeout(timeoutID);
      timeoutID = setTimeout(function () {
        func.apply(scope, Array.prototype.slice.call(args));
      }, timeout);
    };
  }

  /**
   * Initialize readmore areas,
   * using readmore.js jQuery plugin
   *
   * @link(http://jedfoster.com/Readmore.js/)
   */
  function initReadmore() {
    var $readmoreAreas = $('.readmore');

    $readmoreAreas.readmore({
      speed: 180,
      moreLink: '<div class="readmore-toggle"><a href="#">Read more</a> <i class="icon icon-angle-down"></i></div>',
      lessLink: '<div class="readmore-toggle"><a href="#">Close</a> <i class="icon icon-angle-up"></i></div>',
      afterToggle: function (trigger, element, expanded) {
        $(element).toggleClass('expanded', expanded);
      }
    });
  }

  /**
   * Mobile Welcome modal
   * Show it only once through cookies,
   * using jQuery.cookie plugin
   *
   * @link(https://github.com/carhartl/jquery-cookie)
   */
  function initModals () {
    var COOKIE_NAME_WELCOME = 'welcomeMobile';
    var $modalWelcome = $('#modal-welcome');
    var $modalWelcomeBtnContinue = $('#modal-welcome-btn-continue');
    var cookieOnLoad = $.cookie(COOKIE_NAME_WELCOME);

    if (!cookieOnLoad && window.innerWidth < 992) {
      $modalWelcome.modal().on('shown.bs.modal', function () {
        $.cookie(COOKIE_NAME_WELCOME, '1', { expires: 365, path: '/' });
        // maybe analytics tracking
      })
      .on('hide.bs.modal', function () {
        // maybe analytics tracking
      });
      $modalWelcomeBtnContinue.click(function () {
        // maybe analytics tracking
      });
    }

    // remote modals
    $('.modal-remote').click(function (e) {
      var link = this;
      var $modal = $(link.getAttribute('data-modal'));
      var $modalBody = $modal.find('.modal-body');
      $modal.on('show.bs.modal', function () {
        $modalBody.load(link.href);
      })
      .modal();
      e.preventDefault();
    });
  }

  /**
   * Behavior of event list row click
   *
   */
  function initLinks() {
    $('.event-list-item').on('click', function (event) {
      if (!$(event.target).is('a')) {
        var $this = $(this);
        var mainLink = $this.find('a.btn')[0];
        if (mainLink) {
          document.location.href = mainLink.href;
        }
      }
    });
  }

  /**
   * Bootstrap tooltips need to be manually instantiated
   *
   */
  function initTooltips() {
    $('.tip').tooltip({ container: 'body' });
  }

  /**
   * Init fittext
   */
  function initFittext() {
    $('.box-header--title').fitText(2, { minFontSize: '14px', maxFontSize: '30px' });
  }

  /**
   * Manage the responsiveness of certain elements
   * on the website: the sticky header, some grid blocks
   * which has a weird behavior (they are positioned quite
   * differently through the various breakpoints),
   * impossible to obtain through CSS only.
   * We use closures here.
   */
  function initResponsiveness() {

    var $window = $(window);
    var $body = $('body');
    var winWidth = window.innerWidth;
    var winScroll = $window.scrollTop();
    var $whyBook = $('#why-book-with-us');
    var $about = $('#about');
    var $headerTop = $('.header-top');
    var scrollBreakpoint = $headerTop.outerHeight();
    var $stickyHeader = $('.header-to-stick');
    var isStickyCloned = false;
    var isSticked = false;
    var $colTickets = $('#col-tickets');
    var $colStadium = $('#col-stadium');
    var stadiumImg = document.getElementById('stadium-img');
    var stadiumImageHasLoaded = false;
    var $ticketsList = $('#tickets-list');
    var $dropdownMenus = $('#navbar').find('.dropdown-menu');

    /**
     * Create sticky header clone.
     */
    var createStickeHeaderClone = function () {
      $stickyHeader
        .addClass('sticky-header')
        .clone().addClass('sticky-header-clone').insertAfter($stickyHeader);
      isStickyCloned = true;
    };
    /**
     * Manage header stickyness (only on lg screens).
     */
    var manageStickyHeader = function () {
      if (winScroll >= scrollBreakpoint && winWidth >= 992 && !isSticked) {
        if (!isStickyCloned) {
          createStickeHeaderClone();
        }
        $body.addClass('sticky-header-on');
        isSticked = true;
      }
      if (((winScroll < scrollBreakpoint) && isSticked) || winWidth < 992 && isSticked) {
        $body.removeClass('sticky-header-on');
        isSticked = false;
      }
    };
    if (winWidth >= 992) {
      createStickeHeaderClone();
    }
    /**
     * Fix layout grid on tablets.
     * This function is the good place where to put other
     * responsive tweaks.
     */
    var syncBlocksHeight = function () {
      // correspond to: @media (min-width: $screen-sm-min) and (max-width: $screen-sm-max)
      if (winWidth >= 768 && winWidth < 992) {
        $about.css('min-height', $whyBook.outerHeight());
      } else {
        $about.css('min-height', 0);
      }
    };
    /**
     * Give a maximum height to the ticket list
     * Wait for the stadium image to load before to do the calculation
     * or just sync if there is no image.
     */
    var trySyncTicketsPageColsHeight = function () {
      if (!$colTickets.length || !$colStadium.length || !$ticketsList.length) {
        return;
      }
      if (stadiumImg && stadiumImg.src && !stadiumImageHasLoaded) {
        var image = new Image();
        image.src = stadiumImg.src;
        image.onload = maybeSyncTicketsPageColsHeight;
        stadiumImageHasLoaded = true;
      } else {
        maybeSyncTicketsPageColsHeight();
        stadiumImageHasLoaded = true;
      }
    };
    /**
     * Actually sync the maximum height of the ticket list
     * with the column beside only on big screens.
     */
    var maybeSyncTicketsPageColsHeight = function () {
      // correspond to: @media (min-width: $screen-md-min)
      if (winWidth >= 992) {
        var heightLimit = $colStadium.outerHeight();
        var targetHeight = heightLimit - 90;
        $ticketsList.css({
          'max-height': targetHeight
        });
      } else {
        $ticketsList.css({
          'max-height': 'none'
        });
      }
    };
    /**
     * Make the menu dropdowns scrollable
     * if they don't fit in the screen
     */
    var maximumHeightToDropdowns = function () {
      // correspond to: @media (min-width: $grid-float-breakpoint)
      if (winWidth >= 768) {
        $dropdownMenus.css({
          // -20 just to give a little breath in the bottom
          'max-height': window.innerHeight - $stickyHeader.outerHeight() - 20,
          'overflow': 'auto'
        });
      } else {
        $dropdownMenus.css({
          'max-height': 'none',
          'overflow': 'auto'
        });
      }
    };

    // on scroll
    $window.scroll(function() {
      winScroll = $window.scrollTop();
      manageStickyHeader();
    });

    // on resize
    $window.resize(function () {
      winWidth = window.innerWidth;
      syncBlocksHeight();
      manageStickyHeader();
      maximumHeightToDropdowns();
    });
    // on resize (debounced)
    $window.resize(debouncer(function () {
      trySyncTicketsPageColsHeight();
    }));

    // on ready as well
    syncBlocksHeight();
    trySyncTicketsPageColsHeight();
    manageStickyHeader();
    maximumHeightToDropdowns();
  }

  /**
   * On ready
   *
   */
  $(document).ready(function () {
    initResponsiveness();
    initFittext();
    initReadmore();
    initModals();
    initLinks();
    initTooltips();
  });
})(window, document, jQuery);
